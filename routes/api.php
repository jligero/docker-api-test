<?php

Route::namespace('Api')
    ->name('api')
    ->prefix('auth')
    ->group(function () {
        Route::post('/', 'AuthController@login')->name('.auth');
    });

/**
 * Currently, these API routes are not protected.
 * We just test functionallity
 */
Route::namespace('Api\Customers')
    ->name('api')
    ->prefix('customers')
    ->middleware(['auth:api'])
    ->group(function () {
        Route::get('/', 'CustomersController')->name('.customers');
        Route::get('/{id}', 'CustomerController')->name('.customer.read');
        Route::post('/', 'CreateController')->name('.customer.create');
        Route::put('/', 'UpdateController')->name('.customer.update');
        Route::delete('/', 'DeleteController')->name('.customer.delete');
    });

Route::namespace('Api\Users')
    ->name('api')
    ->prefix('users')
    /**
     * @theam the middleware checks all requests before
     * they reach the Controller, thus preventing non-admin
     * Users to reach these routes.
     * 
     * @see App\Http\Middleware\UserIsAdmin
     */
    ->middleware(['is.admin', 'auth:api'])
    ->group(function () {
        Route::get('/', 'UsersController')->name('.users');
        Route::get('/{id}', 'UserController')->name('.user.read');
        Route::post('/', 'CreateController')->name('.user.create');
        Route::put('/', 'UpdateController')->name('.user.update');
        Route::delete('/', 'DeleteController')->name('.user.delete');
    });

