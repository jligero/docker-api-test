<?php 

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Customers;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Customers::class, function (Faker $faker, $args) {
    return [
        'name' => $faker->name,
        'surname' => $faker->unique()->safeEmail,
        'id_code' => Str::random(10),
        'photo' => null,
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
    ];
});
