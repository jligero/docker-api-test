<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedByIdToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            /**
             * @theam I place the foreing keys at the beginning of
             * the table, just to have a clear view on the dependencies.
             * That's the reason I place the updated_by_id after the
             * creator_id.
             */
            $table->unsignedBigInteger('updated_by_id')
                ->nullable()
                ->after('creator_id');
        });

        Schema::table('customers', function (Blueprint $table) {
            $table->foreign('updated_by_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('customers', function (Blueprint $table) {
            $table->dropForeign(['updated_by_id']);
        });
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn(['updated_by_id']);
        });
    }
}
