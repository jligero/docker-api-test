<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueKeyToIdCodeInCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            /**
             * @theam when creating the table, the constraint index()
             * is wrong, it should be unique(). Try to fix that in this 
             * migration.
             */
            $table->string('id_code')->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('customers', function (Blueprint $table) {
            $table->dropUnique('customers_id_code_unique');
        });
    }
}
