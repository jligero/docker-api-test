<?php

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use App\Services\Users\CreateService;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {name} {email} {password} {admin : [true|false]}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admin = (strtolower($this->argument('admin')) === "true" 
            || $this->argument('admin') == 1);
        
        try {
            $createService = new CreateService;
            $user = $createService->fromParams(
                $this->argument('name'),
                $this->argument('email'),
                $this->argument('password'),
                $admin
            );
            $this->comment("User created w/ ID {$user->id}");
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
