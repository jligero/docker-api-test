<?php 

/**
 * @theam this is a helper function to log info
 * in a certain format I like
 */
if (!function_exists("logException")) {
    function logException(\Exception $e)
    {
        \Log::error($e->getMessage());
        \Log::error('File ' . $e->getFile() . ' @line ' . $e->getLine());
        \Log::error($e->getTraceAsString());
    }
}

