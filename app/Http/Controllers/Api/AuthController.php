<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class AuthController extends Controller
{
    private $tokenScope = ['*'];
    public function login(Request $request)
    {
        $attempt = Auth::attempt([
            'email' => $request->email, 
            'password' => $request->password
        ]);
        if ($attempt) { 
            $user = Auth::user(); 
            $token =  $user->createToken('TheamToken', $this->tokenScope)
                ->accessToken; 
            return response()->json([
                'access_token' => $token,
                'scope' => $this->tokenScope,
            ]); 
        }
        return response()->json([], 401); 
    }
}