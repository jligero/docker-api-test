<?php 

namespace App\Http\Controllers\Api\Customers; 

use Exception;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Customers;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function __invoke(Request $request)
    {
        /**
         * @todo add authorization: check the user has the right role
         */

        $customer = Customers::findOrFail($request->id);
        try {
            DB::beginTransaction();
            $customer->delete();
            DB::commit();
            return response()->json([], Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollback();
            logException($e);
            return response()->json(
                $e->getMessage(), 
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
