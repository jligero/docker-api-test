<?php 

namespace App\Http\Controllers\Api\Customers; 

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customers;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    public function __invoke(Request $request)
    {
        /**
         * @todo add authorization
         */
        $customer = Customers::findOrFail($request->id);
        return response()->json($customer->toApiArray(), Response::HTTP_OK);
    }
}
