<?php 

namespace App\Http\Controllers\Api\Customers; 

use Exception;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customers\CreateFormRequest as CreateCustomerFormRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Services\Customers\CreateService as CreateCustomerService;


class CreateController extends Controller
{
    public function __invoke(
        CreateCustomerService $createCustomerService,
        CreateCustomerFormRequest $request
    ) {
        /**
         * @theam at this point, the Request has the required fields 
         */    

        /**
         * @todo add authorization: check the user has the admin role
         */

        try {
            DB::beginTransaction();
            $customer = $createCustomerService->fromRequest(
                auth()->user(),
                $request
            );
            DB::commit();
            return response()->json(
                $customer->toApiArray(), 
                Response::HTTP_CREATED
            );
        } catch (Exception $e) {
            DB::rollback();
            logException($e);
            return response()->json(
                $e->getMessage(), 
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
