<?php 

namespace App\Http\Controllers\Api\Customers; 

use Exception;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Services\Customers\UpdateService as UpdateCustomerService;
use App\Models\Customers;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    public function __invoke(
        Request $request,
        UpdateCustomerService $updateCustomerService
    ) {
        /**
         * @todo add authorization: check the user has the right role
         */

        /**
         * @theam Laravel has an implicit binding that could provide
         * the instance of the Customers Model, taking the ID. But 
         * I rather prefer this explicit declaration that throws a 404
         * if the ID is not found.
         */
        $customer = Customers::findOrFail($request->id);

        try {
            DB::beginTransaction();
            $customer = $updateCustomerService->fromRequest(
                auth()->user(),
                $customer,
                $request
            );
            DB::commit();
            return response()->json(
                $customer->toApiArray(), 
                Response::HTTP_OK
            );
        } catch (Exception $e) {
            DB::rollback();
            logException($e);
            return response()->json(
                $e->getMessage(), 
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
