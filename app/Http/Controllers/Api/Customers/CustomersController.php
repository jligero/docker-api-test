<?php 

namespace App\Http\Controllers\Api\Customers; 

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customers;

class CustomersController extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->json(
            Customers::all()->map(
                function ($customer) {
                    /**
                     * @theam the serialization of the Customer is customized 
                     * using the toApiArray(). Laravel, by default, calls 
                     * the toArray() method of a Model when serializing.
                     * 
                     * I could override the toArray() method in the model, but I 
                     * think it is not a good practice:
                     * 1. Serializing a Model may be useful for several other purposes.
                     * 2. Mixes two different intentions.
                     * 
                     * @see App\Models\Customers
                     */
                    return $customer->toApiArray();
                }
            )
        );
    }
}
