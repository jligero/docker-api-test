<?php 

namespace App\Http\Controllers\Api\Users; 

use Exception;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserFormRequest as CreateUserFormRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Services\Users\CreateService as CreateUserService;
use Illuminate\Support\Facades\Gate;

class CreateController extends Controller
{
    public function __invoke(
        CreateUserService $createUserService,
        CreateUserFormRequest $request
    ) {
        try {
            DB::beginTransaction();
            $user = $createUserService->fromRequest($request);
            DB::commit();
            return response()->json(
                $user->toApiArray(), 
                Response::HTTP_CREATED
            );
        } catch (Exception $e) {
            DB::rollback();
            logException($e);
            return response()->json(
                $e->getMessage(), 
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
