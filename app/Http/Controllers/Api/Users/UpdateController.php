<?php 

namespace App\Http\Controllers\Api\Users; 

use Exception;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Services\Users\UpdateService as UpdateUserService;
use Illuminate\Http\Request;
use App\Models\Users;

class UpdateController extends Controller
{
    public function __invoke(
        Request $request,
        UpdateUserService $updateUserService
    ) {
        $user = Users::findOrFail($request->id);

        try {
            DB::beginTransaction();
            $user = $updateUserService->fromRequest(
                $user,
                $request
            );
            DB::commit();
            return response()->json(
                $user->toApiArray(), 
                Response::HTTP_OK
            );
        } catch (Exception $e) {
            DB::rollback();
            logException($e);
            return response()->json(
                $e->getMessage(), 
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
