<?php 

namespace App\Http\Controllers\Api\Users; 

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;

class UsersController extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->json(
            Users::all()->map(
                function ($user) {
                    return $user->toApiArray();
                }
            )
        );
    }
}
