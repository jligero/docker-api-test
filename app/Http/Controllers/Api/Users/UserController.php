<?php 

namespace App\Http\Controllers\Api\Users; 

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function __invoke(Request $request)
    {
        $user = Users::findOrFail($request->id);
        return response()->json($user->toApiArray(), Response::HTTP_OK);
    }
}
