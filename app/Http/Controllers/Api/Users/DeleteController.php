<?php 

namespace App\Http\Controllers\Api\Users; 

use Exception;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Users;
use Illuminate\Http\Request;

class DeleteController extends Controller
{
    public function __invoke(Request $request)
    {
        /**
         * @note that a User can delete itself!
         */
        $user = Users::findOrFail($request->id);
        try {
            DB::beginTransaction();
            $user->delete();
            DB::commit();
            return response()->json([], Response::HTTP_OK);
        } catch (Exception $e) {
            DB::rollback();
            logException($e);
            return response()->json(
                $e->getMessage(), 
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }
}
