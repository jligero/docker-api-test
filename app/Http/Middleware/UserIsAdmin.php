<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use Closure;

class UserIsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Gate::denies('is-admin')) {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }
        return $next($request);
    }
}
