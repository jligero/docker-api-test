<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * @theam
     * By default, I allways validate.
     * Then, in the Controller I authorize. 
     * For me, this a separation of concerns.
     * By default, Laravel mixes up these two,
     * but I don't like it. I rather prefer to
     * override this behavior by returning true.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|string', // @todo set password rules: length, for example
        ];
    }
}
