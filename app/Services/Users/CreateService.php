<?php 

namespace App\Services\Users;

use Illuminate\Http\Request;
use App\Models\Users;
use Illuminate\Support\Facades\Hash;

class CreateService
{
    public function fromRequest(Request $request) 
    {
        return Users::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
    }

    public function fromParams($name, $email, $password, $isAdmin)
    {
        $user = Users::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        if ($isAdmin) {
            $user->setAsAdmin();
        }

        return $user;
    }
}
