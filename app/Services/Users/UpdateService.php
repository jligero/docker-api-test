<?php 

namespace App\Services\Users;

use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\UserRole;

class UpdateService
{
    /**
     * @theam I assume at this point that all short of validation,
     * and authorization has been done, and the Request contains 
     * the needed parameters.
     */
    public function fromRequest(
        Users $user,
        Request $request
    ) {
        
        if ($request->name) {
            $user->name = $request->name;
        }

        /**
         * @note that we don't check here that
         * the User email is not used. That is 
         * something we rely upon the database 
         */
        if ($request->email) {
            $user->email = $request->email;
        }

        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        if ($request->set_as_admin && ! $user->isAdmin()) {
            $user->setAsAdmin();
        }

        if ($request->unset_as_admin && $user->isAdmin()) {
            $user->unsetAsAdmin();
        }
        
        $user->save();
        
        return $user->fresh();
    }
}