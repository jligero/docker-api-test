<?php 

namespace App\Services\Customers;

use App\Models\Users;
use Illuminate\Http\Request;
use App\Models\Customers;

class CreateService
{
    /**
     * @theam I assume at this point that all short of validation,
     * and authorization has been done, and the Request contains 
     * the needed parameters.
     */
    public function fromRequest(
        Users $creator,
        Request $request
    ) {
        return Customers::create([
            'creator_id' => $creator->id,
            'name' => $request->name,
            'surname' => $request->surname,
            'id_code' => $request->id_code,
        ]);
    }

    /**
     * @theam as an example, this CreateService could have many other
     * functionalities.
     */
    public function fromParameters() 
    {
        return false;
    }
}