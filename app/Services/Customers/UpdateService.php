<?php 

namespace App\Services\Customers;

use App\Models\Users;
use Illuminate\Http\Request;
use App\Models\Customers;

class UpdateService
{
    /**
     * @theam I assume at this point that all short of validation,
     * and authorization has been done, and the Request contains 
     * the needed parameters.
     */
    public function fromRequest(
        Users $updatedBy,
        Customers $customer,
        Request $request
    ) {
        /**
         * @theam The update is done extracting the 
         * array of parameters from the Request and 
         * using Eloquent mass attribute update:
         */
        $customer->update(
            array_merge(
                $request->only(['name', 'surname', 'id_code']),
                ['updated_by_id' => $updatedBy->id]
            )
        );
        
        return $customer->fresh();
    }
}