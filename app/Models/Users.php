<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\UserRole;

class Users extends Authenticatable
{
    use HasApiTokens,
        Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->hasMany(UserRole::class, 'user_id', 'id');
    }

    public function isAdmin()
    {
        return $this->roles->where('role', UserRole::ROLE_ADMIN)->count() !== 0;
    }

    public function setAsAdmin()
    {
        if (!$this->isAdmin()) {
            $this->roles()->create(['role' => UserRole::ROLE_ADMIN]);
            $this->load('roles'); 
        }
        return $this->isAdmin();
    }

    public function unsetAsAdmin()
    {
        if ($this->isAdmin()) {
            $this->roles
                ->where('role', UserRole::ROLE_ADMIN)
                ->first()
                ->delete();   
            $this->load('roles');
        }
        return !$this->isAdmin();
    }

    public function toApiArray()
    {
        return array_merge(
            parent::toArray(),
            ['roles' => $this->roles 
                ? $this->roles->toArray()
                : []
            ]
        );
    }
}
