<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table = 'customers';
    public $timestamps = true;

    protected $fillable = [
        'creator_id', 'updated_by_id', 'name', 'surname', 'id_code', 'photo',
    ];

    public function creator()
    {
        return $this->belongsTo(Users::class, 'creator_id', 'id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(Users::class, 'updated_by_id', 'id');
    }

    public function toApiArray()
    {
        return array_merge(
            ['creator' => $this->creator->toArray()],
            ['updated_by' => $this->updatedBy
                ? $this->updatedBy->toArray()
                : null
            ],
            parent::toArray()
        );
    }
}
