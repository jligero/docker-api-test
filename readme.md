# Hello!
This is the first version of the readme. The idea is to guide you to get the project up and running, and keep track of the progress and what is done and why.

## Installation
1. [Install docker](https://docs.docker.com/install/). It is strongly recommended to install Docker so it does not require `sudo`.
2. [Clone the repo](https://gitlab.com/jligero/docker-api-test). 
```
$ cd to/a/folder
$ git clone https://gitlab.com/jligero/docker-api-test.git docker-api-test
$ cd docker-api-test
```
3. Execute 
```
$ docker run --rm -v $(pwd):/app composer install
$ sudo chown -R $USER:$USER path/to/docker-api-test
```
4. Build and run the containers
```
$ docker-compose build
$ docker-compose up -d
```
5. Laravel still needs some extra configuration:
```
$ cp .env.example .env
$ docker-compose exec app php artisan key:generate
$ docker-compose exec app php artisan config:cache
$ docker-compose exec app php artisan passport:install
```
6. At this point, you could load [http://localhost](http://localhost) and see Laravel's welcome screen.
7. Executing `./phpunit`, an initial test is launched. This test can be seen at `tests/Feature/CustomersControllerTest/CustomersControllerTest.php`. It just creates two customers, a user, using the factories, and checks that the customers are retrieved by the API. 

### Next steps:
1. Frontend to make API handled 
2. Avatar handling

## Update 2019-07-23 (bis)

Application is "human" testeable.
0. If the application was previously installed, you should run:
```
./release-local
```
at least, and maybe 
```
./art passport:install
```
Please, keep at least the ID of the "Personal access client". It may be needed in step 5.
1. Create a User, using an Artisan command (created):
```
[if not in the container] docker-compose exec app bash
php artisan user:create "John Doe" "jdoe@gmail.com" "s3cr3t" "true"
```
2. Generate Personal access client ID (if not done before) 
```
./art passport:client --personal
```
When asked for the name of the personal access client, hit enter (default)
4. Keep the Client ID and Client secret
5. If the Client ID is different than 1, edit `app/Providers/AuthServiceProvider.php`, line 37, and update the value.
6. Recommended final step:
```
php artisan config:clear
```
7. You can request an access token in `api/auth` for the User created in step 1: usue a POST with the email and password in the body of the request to the previous route.
8. When usuing a request to the server in a protected route, you should add the following Bearer Token authorization header:
```
Authorization: Bearer the-token-goes-here-!
```
9. On localhost:8080 you can access a PHPMyAdmin to create

## Update 2019-07-23

1. Tests were updated
2. API routes protected with `auth:api` middleware (OAUTH2)

## Update 2019-07-22

1. To have Passport running
```
./release-local
php artisan passport:install
``` 

## Update 2019-07-21

1. User management: only Users having the administrator role, can manage Users.
2. Administrators can set/unset other User as administrators.
3. Routes for User management are protected with a Middleware, so each request is checked before reaching the Controller.
4. Relevant places to look into:
```
app/Http/Controllers/Api/Users
app/Http/Kernel.php
app/Http/Middleware/UserIsAdmin.php
app/Models/UserRole.php
app/Providers/AuthServiceProvider.php
routes/api.php
tests/Feature/Api
tests/Unit
```

## Update 2019-07-18

1. Delete and read of Customer is now possible. Still, no image, no authentication, 
no authorization.
2. Laravel, when a Model is not found, generates a full HTTP response, not just a JSON response. I had to modify `app/Exceptions/Handler.php` to introduce this behavior.
3. Naming of the methods in `tests/Feature/Api/RoutesTrait.php` were changed to be shorter and more descriptive. Tests where changed accordingly.

## Update 2019-07-17

1. As a dependency was needed, a script was created to handle the release on local environment. After a `git pull`, execute `./release-local`.
2. Search in the project for `@theam` for relevant comments.
3. Added the CreationController and UpdateController. No authentication, nor authorization still added. Customer image is not yet handled. Roles for the Users are not set.
4. Relevant places to look into:
```
app/Http/Controllers/Api/Customers
app/Http/Requests
app/Models
app/Services/Customers
database/factories
database/migrations
routes/api.php
tests/Feature/Api

```

### Next steps:
1. Delete Customers
2. Add roles to Users
3. Avatar handling
