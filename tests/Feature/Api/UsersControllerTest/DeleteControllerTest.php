<?php 

namespace Tests\Feature\Api\UsersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;

class DeleteControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait,
        WithFaker;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
        
        $this->admin = factory(Users::class)->create();
        $this->admin->setAsAdmin();
    }

    public function testCanNotDeleteUnknownUser()
    {
        // Arrange
        Passport::actingAs($this->admin, ['*']);
        
        // Act
        $response = $this->deleteUser(['id' => 'unknow_id']);
        
        // Assert
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJsonFragment([]);
    }

    public function testCanNotDeleteUser()
    {
        // Arrange
        Passport::actingAs($this->user, ['*']);
        
        // Act
        $response = $this->deleteUser(['id' => $this->user->id]);
        
        // Assert
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testCanDeleteCustomer()
    {
        // Arrange
        Passport::actingAs($this->admin, ['*']);
        
        // Act
        $response = $this->deleteUser(['id' => $this->user->id]);
        
        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([]);
        $this->assertDatabaseMissing('users', ['id' => $this->user->id]);
    }
}