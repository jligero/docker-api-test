<?php

namespace Tests\Feature\Api\UsersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Passport\Passport;

class UserControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
        $this->admin = factory(Users::class)->create();
        $this->admin->setAsAdmin();
    }

    public function testNotFoundCustomer()
    {
        // Arrange 
        Passport::actingAs($this->admin, ['*']);

        // Act
        $response = $this->getUser('not_an_id');

        // Assert
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJsonFragment([]);
    }

    public function testNonAdminCanNotReadAUser()
    {
        // Arrange 
        Passport::actingAs($this->user, ['*']);

        // Act
        $response = $this->getUser($this->user->id);

        // Assert
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testAdminCanReadAUser()
    {
        // Arrange 
        Passport::actingAs($this->admin, ['*']);

        // Act
        $response = $this->getUser($this->user->id);

        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment($this->user->toApiArray());
    }
}
