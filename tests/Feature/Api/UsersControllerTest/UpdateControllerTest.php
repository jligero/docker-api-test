<?php 

namespace Tests\Feature\Api\UsersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\UserRole;
use Laravel\Passport\Passport;

class UpdateControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait,
        WithFaker;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
        $this->admin = factory(Users::class)->create();
        $this->admin->setAsAdmin();
    }

    public function testNonAdminCanNotUpdateUser()
    {
        // Arrange
        Passport::actingAs($this->user, ['*']);
        $userData = array_merge(
            $this->user->toArray(),
            ['name' => 'Known name'],
            ['surname' => 'Known surname']
        );

        // Act
        $response = $this->updateUser($userData);
        
        // Assert
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testAdminCanUpdateUser()
    {
        // Arrange
        Passport::actingAs($this->admin, ['*']);
        $newEmail = $this->faker->email();
        $userData = array_merge(
            $this->user->toArray(),
            ['name' => 'Known name'],
            ['email' => $newEmail]
        );

        // Act
        $response = $this->updateUser($userData);
        
        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment($userData);
    }

    public function testAdminCanSetUserAsAdmin()
    {
        // Arrange
        Passport::actingAs($this->admin, ['*']);

        // Act
        $response = $this->updateUser(
            [
                'id' => $this->user->id,
                'set_as_admin' => true,
            ]
        );
        
        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas(
            'user_role', 
            [
                'user_id' => $this->user->id,
                'role' => UserRole::ROLE_ADMIN
            ]
        );
    }

    public function testAdminCanUnsetUserAsAdmin()
    {
        // Arrange
        Passport::actingAs($this->admin, ['*']);

        // Act
        $response = $this->updateUser(
            [
                'id' => $this->user->id,
                'unset_as_admin' => true,
            ]
        );
        
        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing(
            'user_role', 
            [
                'user_id' => $this->user->id,
                'role' => UserRole::ROLE_ADMIN
            ]
        );
    }
}
