<?php

namespace Tests\Feature\Api\UsersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Passport\Passport;

class UsersControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
    }

    public function testNonAdmin()
    {
        // Arrange 
        Passport::actingAs($this->user, ['*']);

        // Act
        $response = $this->getUsers();

        // Assert
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testAllUsersAreListedIfUserIsAdmin()
    {
        // Arrange 
        $this->user->setAsAdmin(); // User is promoted!
        Passport::actingAs($this->user, ['*']);
        $users = array_merge(
            [$this->user->toApiArray()],
            // Serialize created Users with toApiArray()
            factory(Users::class, 2)->create()->map(function ($user) {
                return $user->toApiArray();
            })
            ->toArray()
        );

        // Act
        $response = $this->getUsers();

        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonCount(count($users));
        $response->assertJsonFragment($users[0]);
        $response->assertJsonFragment($users[1]);
        $response->assertJsonFragment($users[2]);
    }
}
