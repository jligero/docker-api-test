<?php 

namespace Tests\Feature\Api\UsersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;

class CreateControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait,
        WithFaker;

    public function setUp():void
    {
        parent::setUp();

        $this->admin = factory(Users::class)->create();
        $this->admin->setAsAdmin();

        $this->nonAdmin = factory(Users::class)->create();
    }

    public function testNonLoggedUserCanNotCreateNewUser()
    {
        // Arrange 
        $userData = [
            'name' => $this->faker->firstName(),
            'email' => $this->faker->email(),
            'password' => $this->faker->realText(10),
        ];

        // Act
        $response = $this->createUser($userData);
        
        // Assert
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testNonAdminUserCanNotCreateNewUser()
    {
        // Arrange 
        Passport::actingAs($this->nonAdmin, ['*']);

        // Act
        $response = $this->createUser([]);
        
        // Assert
        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testAdminUserCanNotCreateNewUserIfValidatioFails()
    {
        // Arrange 
        Passport::actingAs($this->admin, ['*']);
        $userData = [
            'name' => $this->faker->firstName(),
            'email' => $this->faker->email(),
            // Password is missing
        ];

        // Act
        $response = $this->createUser($userData);
        
        // Assert
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testAdminUserCanCreateNewUser()
    {
        // Arrange 
        Passport::actingAs($this->admin, ['*']);
        $userData = [
            'name' => $this->faker->firstName(),
            'email' => $this->faker->email(),
        ];

        // Act
        $response = $this->createUser(
            array_merge(
                $userData,
                /**
                 * Password is not serialized in the respone
                 * so I just split the definition.
                 */
                ['password' => $this->faker->realText(10)]
            )
        );
        
        // Assert
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonFragment($userData);
        $this->assertDatabaseHas('users', $userData);
    }
}