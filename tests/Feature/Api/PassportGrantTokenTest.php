<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use Laravel\Passport\Passport;
use Tests\Feature\Api\RoutesTrait;

class PassportGrantTokenTest extends TestCase
{
    use RefreshDatabase,
        RoutesTrait;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
    }

    public function testPassportFails()
    {
        // Arrange
        Passport::actingAs($this->user, ['*']);
        
        // Act
        $response = $response = $this->getUsers();
        
        // Assert
        $response->assertStatus(403);
    }
    
    public function testPassportAuthSuccess()
    {
        // Arrange
        $this->user->setAsAdmin();
        Passport::actingAs(
            $this->user,
            ['*']
        );
        
        // Act
        $response = $response = $this->getUsers();
        
        // Assert
        $response->assertStatus(200);
    }
}