<?php 

namespace Tests\Feature\Api\CustomersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;

class CreateControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait,
        WithFaker;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
    }

    public function testCanNotCreateCustomer()
    {
        // Arrange 
        Passport::actingAs($this->user, ['*']);
        $customerData = [];

        // Act
        $response = $this->createCustomer($customerData);
        
        // Assert
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testCreateCustomer()
    {
        // Arrange
        Passport::actingAs($this->user, ['*']);
        $customerData = [
            'name' => $this->faker->firstName(),
            'surname' => $this->faker->lastName(),
            'id_code' => $this->faker->swiftBicNumber(),
        ];

        // Act
        $response = $this->createCustomer($customerData);

        /** 
         * Assert
         */
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJsonFragment($customerData);
        $response->assertJsonFragment(['creator' => $this->user->toApiArray()]);
        // Assert that the customer has been persisted
        $this->assertDatabaseHas(
            'customers',
            array_merge(
                $customerData,
                ['creator_id' => $this->user->id]
            ) 
        );
    }
}