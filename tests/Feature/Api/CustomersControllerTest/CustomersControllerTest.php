<?php

namespace Tests\Feature\Api\CustomersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use App\Models\Customers;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Laravel\Passport\Passport;

class CustomersControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
    }

    public function testCreatedCustomersAreListed()
    {
        // Arrange 
        Passport::actingAs($this->user, ['*']);
        $customers = factory(Customers::class, 2)->create([
            'creator_id' => $this->user->id, 
        ]);

        // Act
        $response = $this->getCustomers();

        // Assert
        $response->assertStatus(200);
        $response->assertJsonCount(count($customers));
        $response->assertJsonFragment($customers[0]->toApiArray());
        $response->assertJsonFragment($customers[1]->toApiArray());
    }
}
