<?php 

namespace Tests\Feature\Api\CustomersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Customers;
use Laravel\Passport\Passport;

class DeleteControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait,
        WithFaker;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
    }

    public function testCanNotDeleteCustomer()
    {
        // Arrange
        Passport::actingAs($this->user, ['*']);
        
        // Act
        $response = $this->deleteCustomer(['id' => 'not_an_id']);
        
        // Assert
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJsonFragment([]);
    }

    public function testCanDeleteCustomer()
    {
        // Arrange
        Passport::actingAs($this->user, ['*']);
        $customer = factory(Customers::class)->create([
            'creator_id' => $this->user->id,
        ]);
        
        // Act
        $response = $this->deleteCustomer(['id' => $customer->id]);
        
        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment([]);
        $this->assertDatabaseMissing('customers', ['id' => $customer->id]);
    }
}