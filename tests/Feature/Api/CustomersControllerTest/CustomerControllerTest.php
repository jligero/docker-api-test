<?php

namespace Tests\Feature\Api\CustomersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use App\Models\Customers;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Symfony\Component\HttpFoundation\Response;
use Laravel\Passport\Passport;

class CustomerControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
    }

    public function testNotFoundCustomer()
    {
        // Arrange 
        Passport::actingAs($this->user, ['*']);

        // Act
        $response = $this->getCustomer('not_an_id');

        // Assert
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJsonFragment([]);
    }

    public function testReadACustomer()
    {
        // Arrange 
        Passport::actingAs($this->user, ['*']);
        $customers = factory(Customers::class, 2)->create([
            'creator_id' => $this->user->id, 
        ]);

        // Act
        $response = $this->getCustomer($customers[0]->id);

        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment(['id_code' => $customers[0]->id_code]);
        $response->assertJsonMissing(['id_code' => $customers[1]->id_code]);
    }
}
