<?php 

namespace Tests\Feature\Api\CustomersControllerTest;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Users;
use Tests\Feature\Api\RoutesTrait as ApiRoutesTrait;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Customers;
use Laravel\Passport\Passport;

class UpdateControllerTest extends TestCase
{
    use RefreshDatabase,
        ApiRoutesTrait,
        WithFaker;

    public function setUp():void
    {
        parent::setUp();

        $this->user = factory(Users::class)->create();
        $this->user2 = factory(Users::class)->create();
    }

    public function testCanUpdateCustomer()
    {
        // Arrange
        Passport::actingAs($this->user, ['*']);
        $customer = factory(Customers::class)->create([
            'creator_id' => $this->user->id,
        ]);
        $customerData = array_merge(
            $customer->toArray(),
            ['name' => 'Known name'],
            ['surname' => 'Known surname']
        );

        // Act
        $response = $this->updateCustomer($customerData);
        
        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment($customerData);
        $response->assertJsonFragment(['creator_id' => $this->user->id]);
        $response->assertJsonFragment(['updated_by_id' => $this->user->id]);
    }

    public function testUpdatedByIsUpdated()
    {
        /**
         * Arrange
         */
        Passport::actingAs($this->user, ['*']);
        $customerData = [
            'name' => $this->faker->firstName(),
            'surname' => $this->faker->lastName(),
            'id_code' => $this->faker->swiftBicNumber(),
        ];
        // Creation is via API
        $response = $this->createCustomer($customerData);
        // Change user updating
        Passport::actingAs($this->user2, ['*']);
        // Change customer data
        $createdCustomer = (object) $response->decodeResponseJson();
        $customerData = [
            'id' => $createdCustomer->id,
            'name' => 'Known name',
            'surname' => 'Known surname',
        ];

        // Act
        $response = $this->updateCustomer($customerData);
        
        // Assert
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment($customerData);
        $response->assertJsonFragment(['creator_id' => $this->user->id]);
        $response->assertJsonFragment(['updated_by_id' => $this->user2->id]);
    }
}