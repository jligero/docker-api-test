<?php 

namespace Tests\Feature\Api;

trait RoutesTrait
{
    /**
     * Customers
     */

    public function getCustomers()
    {
        return $this->json(
            'GET', 
            route('api.customers')
        );
    }

    public function getCustomer($id)
    {
        return $this->json(
            'GET',
            route('api.customer.read', ['id' => $id])
        );
    }

    public function createCustomer($params)
    {
        return $this->json(
            'POST',
            route('api.customer.create'),
            $params
        );
    }

    public function updateCustomer($params)
    {
        return $this->json(
            'PUT',
            route('api.customer.update'),
            $params
        );
    }

    public function deleteCustomer($params)
    {
        return $this->json(
            'DELETE',
            route('api.customer.delete'),
            $params
        );
    }

    /**
     * Users
     */

    public function getUsers()
    {
        return $this->json(
            'GET', 
            route('api.users')
        );
    }

    public function getUser($id)
    {
        return $this->json(
            'GET',
            route('api.user.read', ['id' => $id])
        );
    }

    public function createUser($params)
    {
        return $this->json(
            'POST',
            route('api.user.create'),
            $params
        );
    }

    public function updateUser($params)
    {
        return $this->json(
            'PUT',
            route('api.user.update'),
            $params
        );
    }

    public function deleteUser($params)
    {
        return $this->json(
            'DELETE',
            route('api.user.delete'),
            $params
        );
    }
}