<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Users;

class AssignedRoleTest extends TestCase
{
    use RefreshDatabase,
        WithFaker;
    
    public function testAssignedAdminRoleIsReadAsAdmin()
    {
        // Arrange
        $user = factory(Users::class)->create();
        
        // Act
        $done = $user->setAsAdmin();
        
        // Assert
        $this->assertTrue($done);
        $this->assertTrue($user->isAdmin());
    }

    public function testNonAssignedAdminRoleIsNotReadAsAdmin()
    {
        // Arrange
        $user = factory(Users::class)->create();
        
        // Act

        // Assert
        $this->assertTrue(false === $user->isAdmin());
    }

    public function testAssignedAdminRoleCanBeUnnassigned()
    {
        // Arrange
        $user = factory(Users::class)->create();
        $user->setAsAdmin();
        
        // Act
        $done = $user->unsetAsAdmin();
        
        // Assert
        $this->assertTrue($done);
        $this->assertTrue(false === $user->isAdmin());
    }
}